file=$1


#######################################################################
assert ()                 #  If condition false,
{                         #+ exit from script
                          #+ with appropriate error message.
  echo "assert $1 $2"
  E_PARAM_ERR=98
  E_ASSERT_FAILED=99


  if [ -z "$2" ]          #  Not enough parameters passed
  then                    #+ to assert() function.
    lineno="Line number unknown."
    # return $E_PARAM_ERR   #  No damage done.
  else
    lineno=$2
  fi

  if [ ! $1 ]
  then
    echo "Assertion failed:  \"$1\""
    echo "File \"$0\", line $lineno"    # Give name of file and line number.
    exit $E_ASSERT_FAILED
  else
    echo "assert on line $lineno passed"
  #   return
  #   and continue executing the script.
  fi
} # Insert a similar assert() function into a script you need to debug.
#######################################################################

# $LINENO is bash-specific, so using assert 0 $LINENO to report the line number of the assertion only works in bash.

contains() { if [ "${1##"$2"}" ]; then return 1; else return 0; fi }

contains "#" "# a comment"
assert $? 37
if contains "#" "etoolbox"; then
  exit 99
fi
#assert $(1 - ?) 38
#assert !$? 38

contains "erun" "LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right."
assert $? "erun"

# http://mywiki.wooledge.org/BashFAQ/001
while IFS= read -r line; do
  if contains "#" $line; then
    printf "$line\n";
  else
    printf 'tlmgr install %s\n' "$line"
    tlmgr info --only-installed $line || tlmgr install $line
  fi
done < "$file"
